cc.Class({
    extends: cc.Component,

    properties: {
        pfOpenItem:{
            default:null,
            type:cc.Prefab
        },

        pfBaseItem:{
            default:null,
            type:cc.Prefab
        },

        pfBase1Item:{
            default:null,
            type:cc.Prefab
        },

        pfIssueItem:{
            default:null,
            type:cc.Prefab
        },

        
        pfNumItem:{
            default:null,
            type:cc.Prefab
        },

        pfContentItem:{
            default:null,
            type:cc.Prefab
        },

       fbHotItem:{
            default:null,
            type:cc.Prefab
        },

        //开奖界面
        ndOpenPanle:{
            default:null,
            type:cc.Node
        },

        //走势界面
        ndBasePanle:{
            default:null,
            type:cc.Node
        },

        swContent:{
            default:[],
            type:cc.ScrollView
        },
        
        //和值走势界面
        ndSumContent:{
            default:[],
            type:cc.Node
        },

        //冷热界面
        ndHotContent:{
            default:null,
            type:cc.Node
        },

        spSort:{
            default:[],
            type:cc.SpriteFrame
        },

        _frontTg:null,
        _data:null,
        _reaCount: 18,
        _spacing: 4,
        _totalCount: 50,
        _openItemArr: [],
        _addType: false,
        _addType1: false
    },

    // use this for initialization
    onLoad: function () {
        var self = this;
        this.showPage(this._data);
    },

    init:function(data){
        this._data = data;
    },

    showPage:function(info){
     //   cc.log("trend showpage");
        this._color =[];
         var color = [];
        var color1 = new cc.Color(253, 253, 251);
        var color2 = new cc.Color(246, 247, 241);
        color.push(color1);
        color.push(color2);
        this._color.push(color1);
        this._color.push(color2);

        var tempinfo = eval('('+ info +')');
        var trdata = tempinfo["tr"]; 
        
        this._openData = trdata;
        this._totalCount =this._openData.length;
        this.lastPositionY =0;
        this.buffer =730;

        for(var i=0;i<this._reaCount;++i){
            var openItem =cc.instantiate(this.pfOpenItem);
            openItem.setPosition(0,-openItem.height*(i)-this._spacing*(i+1));
            var data = {
                Isuse:this._openData[i].i,
                num1:this._openData[i].n,
                num2:this._openData[i].s,
                num3:this._openData[i].dx,
                num4:this._openData[i].ds,
                color:color[i%2]
            };
            openItem.getComponent(openItem.name).onItemIndex(i);
            openItem.getComponent(openItem.name).init(data); 
            this.ndOpenPanle.addChild(openItem);
            this._openItemArr.push(openItem);
        }
  
        this.openItemHeight =openItem.height;
        this.ndOpenPanle.height =this._totalCount*(this.openItemHeight+this._spacing)+this._spacing;

        this.baseItemArr=[];
        this.baseItemArr1 =[];
        this.contentItemArr0 =[];
        this.contentItemArr=[];
        this.contentItemArr1=[];
        this.contentItemArr2=[];
        this.contentItemArr3=[];
        for(var i=0;i<this._openData.length;i++)
        {
             //基本走势
            var baseItem = cc.instantiate(this.pfBaseItem);
            var data = {
                Isuse:this._openData[i].i,
                num1:this._openData[i].n,
                num2:this._openData[i].s,
                num3:this._openData[i].sp,
                num4:this._openData[i].bt,
                color:color[i%2]
            };
            baseItem.getComponent(baseItem.name).init(data);
            this.baseItemArr.push(baseItem);

            //和值走势
            var issueItem = cc.instantiate(this.pfIssueItem);
            issueItem.color = color[i%2];
            issueItem.getChildByName("labIssue").getComponent(cc.Label).string = this._openData[i].i.substring(this._openData[i].i.length-2,this._openData[i].i.length) + "期";;
            this.contentItemArr0.push(issueItem);

            var contentItem = cc.instantiate(this.pfContentItem);
            var sumdata = {
                type:3,
                rt:this._openData[i].t,
                color:color[i%2],
                numColor:null,
                isSpecial:0,
            }
            contentItem.getComponent(contentItem.name).init(sumdata);
            this.contentItemArr.push(contentItem);
            
        }

         //统计出现期数、最大、平均遗漏、最大连出
        var censusStr = ["出现次数","平均遗漏","最大遗漏","最大连出"];
        var censtrCor1 = new cc.Color(122,30,150);
        var censtrCor2 = new cc.Color(37,87,0);
        var censtrCor3 = new cc.Color(110,35,0);
        var censtrCor4 = new cc.Color(0,96,132);
        var censtrCor5 = new cc.Color(229,225,214);
        var censtrCor6 = new cc.Color(234,233,229);
        var colorBg = [censtrCor5,censtrCor6];
        var colorStr = [censtrCor1,censtrCor2,censtrCor3,censtrCor4];

        var bnData = tempinfo["bn"];
        var baData = tempinfo["ba"];
        var bmData = tempinfo["bm"];
        var bsData = tempinfo["bs"];
        var bnamsList = [];
        bnamsList.push(bnData);
        bnamsList.push(baData);
        bnamsList.push(bmData);
        bnamsList.push(bsData);

        var snData = tempinfo["sn"];
        var saData = tempinfo["sa"];
        var smData = tempinfo["sm"];
        var ssData = tempinfo["ss"];
        var snamsList = [];
        snamsList.push(snData);
        snamsList.push(saData);
        snamsList.push(smData);
        snamsList.push(ssData);
     //   cc.log('snamsList数据',snamsList);

        for(var i=0;i<4;i++)
        {
            //基本走势统计
            var base1Item = cc.instantiate(this.pfBase1Item);
            var data = {
                color:color[i%2],
                dec:censusStr[i],
                decColor:colorStr[i],
                decBgColor:colorBg[i%2],
                nums:bnamsList[i],
                numColor:colorStr[i]
            }
            base1Item.getComponent(base1Item.name).init(data);
            this.baseItemArr1.push(base1Item);

            //和值走势
            var issueItem2 = cc.instantiate(this.pfIssueItem);
            issueItem2.color = colorBg[(i+this._openData.length)%2];
            issueItem2.getChildByName("labIssue").color = colorStr[i];
            issueItem2.getChildByName("labIssue").getComponent(cc.Label).string = censusStr[i];
            this.contentItemArr2.push(issueItem2);

            var contentItem = cc.instantiate(this.pfContentItem);
            var sumdata = {
                type:3,
                rt:snamsList[i],
                color:color[i%2],
                numColor:colorStr[i],
                isSpecial:1,
            }
            contentItem.getComponent(contentItem.name).init(sumdata);
            this.contentItemArr3.push(contentItem);
        }

        //冷热
        var oData = tempinfo["o"];
        var o3Data = tempinfo["o3"];
        var o5Data = tempinfo["o5"];
        var o0Data = tempinfo["o0"];
        this.hotData= [];
        for(var i=3;i<19;i++)
        {
            //和值走势
            var numItem = cc.instantiate(this.pfNumItem);
            numItem.getChildByName("labNum").getComponent(cc.Label).string = i;
            this.ndSumContent[1].addChild(numItem);

            //冷热
            var data = {
                type:3,
                num:i,
                issue30:o3Data[i-3],
                issue50:o5Data[i-3],
                issue0:o0Data[i-3],
                miss:oData[i-3],
                color:color[i%2]
            }
  
            this.hotData.push(data);
            var hotItem = cc.instantiate(this.fbHotItem);
            hotItem.getComponent(hotItem.name).init(data);
            this.ndHotContent.addChild(hotItem);
        }

    },

    //开奖期号排序
    issueSort:function(toggle){
        this.ndOpenPanle.parent.parent.getComponent(cc.ScrollView).scrollToTop(0.1);
        if(toggle.getComponent(cc.Toggle).isChecked){
            this._openData =this._openData.reverse(); 
        }
        else{
            this._openData =this._openData.reverse();
        }
       
        for(var i=0;i<this._openItemArr.length;++i){
            this._openItemArr[i].setPosition(0,- this._openItemArr[i].height*(i)-this._spacing*(i+1));
            var data = {
                Isuse:this._openData[i].i,
                num1:this._openData[i].n,
                num2:this._openData[i].s,
                num3:this._openData[i].dx,
                num4:this._openData[i].ds,
                color:this._color[i%2]
            };
            this._openItemArr[i].getComponent(this._openItemArr[i].name).onItemIndex(i);
            this._openItemArr[i].getComponent(this._openItemArr[i].name).updateData(data); 
        }
    },

    //冷热排序
    onAfterSort:function(toggle,customEventData){
        this.hotSort(toggle,this.ndHotContent,this.hotData,customEventData);
    },

    hotSort:function(toggle,panle,arry,key){
        if(arry == null)
            return;
        toggle.target.getComponent(cc.Sprite).spriteFrame = this.spSort[0];
        if(this._frontTg!=null)
            this._frontTg.target.getComponent(cc.Sprite).spriteFrame = this.spSort[0];
        this._frontTg = toggle;

        var children = panle.children;
        var childrenLen = children.length;
        
        var keyStr = "";
        switch (key)
        {
            case "1"://号码
            {
                if(toggle.getComponent(cc.Toggle).isChecked)    
                {
                    Utils.sortByKey(arry,"num",true);
                    if(childrenLen == arry.length)
                    {
                        for (var i = 0; i < childrenLen; ++i) {
                            children[i].getComponent(children[i].name).updateData(arry[i]);
                        }
                    }
                }
                else
                {
                    Utils.sortByKey(arry,"num",false);
                    if(childrenLen == arry.length)
                    {
                        for (var i = 0; i < childrenLen; ++i) {
                            children[i].getComponent(children[i].name).updateData(arry[i]);
                        }
                    }
                    toggle.target.getComponent(cc.Sprite).spriteFrame = this.spSort[2];
                }
            }
            break;
            case "2"://30期
            {
                keyStr = "issue30";
            }
            break;
            case "3"://50期
            {
                keyStr = "issue50";
            }
            break;
            case "4"://100期
            {
                keyStr = "issue0";
            }
            break;
            case "5"://遗漏
            {
                keyStr = "miss";
            }
            break;
            default:
            break;
        }

        if(keyStr != "")
        {
            if(toggle.getComponent(cc.Toggle).isChecked)    
            {
                Utils.sortByKey(arry,keyStr,false);
                if(childrenLen == arry.length)
                {
                    for (var i = 0; i < childrenLen; ++i) {
                        children[i].getComponent(children[i].name).updateData(arry[i]);
                    }
                }
                
            }
            else
            {
                Utils.sortByKey(arry,keyStr,true);
                if(childrenLen == arry.length)
                {
                    for (var i = 0; i < childrenLen; ++i) {
                        children[i].getComponent(children[i].name).updateData(arry[i]);
                    }
                }
                toggle.target.getComponent(cc.Sprite).spriteFrame = this.spSort[1];
            }
        } 
    },


    onScrollowPanle1:function(scrollview, eventType){
      //  cc.log("scrol-0");
        var offset = scrollview.getScrollOffset();
        this.swContent[2].scrollToOffset(offset);
    },

    onScrollowPanle2:function(scrollview, eventType){
     //   cc.log("scrol-1");
        var offset = scrollview.getScrollOffset();
        var offx = Math.abs(offset.x);
        this.swContent[2].scrollToOffset(cc.p(offx,offset.y));
    },

    onScrollowPanle3:function(scrollview, eventType){
     //   cc.log("scrol-2");
        var offset = scrollview.getScrollOffset();
        var offx = 0 - offset.x;
        this.swContent[0].scrollToOffset(offset);
        this.swContent[1].scrollToOffset(cc.p(offx,offset.y));
    },

    onClose:function(){
         
    },

    scrollCallBackFun: function(){
        if(!this._openItemArr.length){
            return;
        }else{
            Utils.preNodeComplex(this.ndOpenPanle,this.openItemHeight,this._spacing,this._reaCount,this._openItemArr,this.buffer,this._openData,this._color,'trend_opencontent_item'); 
        }
    },

    //----切换到那个单选按钮，才将它们addChild到舞台上来
    //基本走势
    tgBaseTrend:function(){
        if(this._addType){return}
        for(var i=0;i<this.baseItemArr.length;++i){
            this.ndBasePanle.addChild(this.baseItemArr[i]);  
            this._addType =true; 
        }
        for(var i=0;i<this.baseItemArr1.length;++i){
            this.ndBasePanle.addChild(this.baseItemArr1[i]);
            this._addType =true;    
        }
    },

    //和值走势
    tgSumTrend: function(){
        if(this._addType1){return}
        for(var i=0;i<this.contentItemArr0.length;++i){
            this.ndSumContent[0].addChild(this.contentItemArr0[i]);
            this._addType1 =true;
        } 
        for(var i=0;i<this.contentItemArr.length;++i){
            this.ndSumContent[2].addChild(this.contentItemArr[i]); 
            this._addType1 =true;   
        }  
        for(var i=0;i<this.contentItemArr2.length;++i){
            this.ndSumContent[0].addChild(this.contentItemArr2[i]);   
            this._addType1 =true;   
        }  
        for(var i=0;i<this.contentItemArr3.length;++i){
            this.ndSumContent[2].addChild(this.contentItemArr3[i]);
            this._addType1 =true;
        }
    }

});
