/**
 * !#zh 快3走势图形态走势组件
 * @information 开奖号/三同号/三不同号/二同号/二不同号 
 */
cc.Class({
    extends: cc.Component,

    properties: {
        labIssue:{
            default:null,
            type:cc.Label
        },
        labOpenNums:{
            default:null,
            type:cc.Label
        },
        labNums:{
            default:[],
            type:cc.Label
        },

        ndNumBg:{
            default:[],
            type:cc.Node
        },

        ndBg:{
            default:null,
            type:cc.Node
        },
        _data:null,
        _ruleDec:[]
    },

    // use this for initialization
    onLoad: function () {
        if(this._data != null ){
            this.ndBg.color = this._data.color;
            this._ruleDec = ["三同号","三不同","二同号","二不同"];
            var color = [];
            var color1 = new cc.Color(149, 205, 131);
            var color2 = new cc.Color(161, 255, 189);
            var color3 = new cc.Color(255, 244, 176);
            var color4 = new cc.Color(244, 184, 90);
            color.push(color1);
            color.push(color2);
            color.push(color3);
            color.push(color4);
            var isuseStr = this._data.Isuse.substring(this._data.Isuse.length-2,this._data.Isuse.length) + "期";
            this.labIssue.string = isuseStr;
            var openStr = "";
            for(var i=0;i<this._data.openNums.length;i++)
            {
                openStr += this._data.openNums[i]+" ";
            }
            this.labOpenNums.string = openStr;

            for(var i=0;i<this._data.nums.length;i++ )
            {
                this.labNums[i].string = this._data.nums[i]==0?this.setSign(i,this._data.nums[i],color[i]):this._data.nums[i];
            }
         }
    },

    /** 
    * 不同节点的显示及颜色
    * @method setSign
    * @param {Number,Number,Object} index,num,color
    * @return {String} 节点显示出来的文字
    */
    setSign:function(index,num,color){
        this.ndNumBg[index].color = color;
        this.ndNumBg[index].active = true;
        return this._ruleDec[index];
    },

    /** 
    * 接收走势图形态走势信息
    * @method init
    * @param {Object} data
    */
    init:function(data){
        this._data = data;
    }
    
});
