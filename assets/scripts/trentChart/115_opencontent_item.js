/**
 * !#zh 11选5走势图开奖组件 
 * @information 期号，开奖号码，和值，跨度，重号个数
 */
cc.Class({
    extends: cc.Component,

    properties: {
        rtOpenNum:{
            default:null,
            type:cc.RichText
        },
        labIssue:{
            default:null,
            type:cc.Label
        },
        labSum:{
            default:null,
            type:cc.Label
        },
        labSpan:{
            default:null,
            type:cc.Label
        },
        labRepeat:{
            default:null,
            type:cc.Label
        },

        _itemId:0,
        _data:null
    },

    // use this for initialization
    onLoad: function () {
        if(this._data != null)
        {
            this.node.color = this._data.color;
            var isus =  this._data.Isuse.toString();
            var isuseStr = isus.substring(isus.length-2,isus.length) + "期";
            this.labIssue.string = isuseStr;
            var nums = "";
            switch (this._data.type)
            {
                case 0:
                {
                    for(var i=0;i<this._data.openNums.length;i++)
                    {
                        var numTemp = this._data.openNums[i]>9?this._data.openNums[i].toString():"0" + this._data.openNums[i];
                        nums+=numTemp+" ";
                    }
                }
                break;
                case 1:
                case 2:
                case 3:
                {
                    for(var i=0;i<this._data.openNums.length;i++)
                    {
                        var numTemp = this._data.openNums[i]>9?this._data.openNums[i].toString():"0" + this._data.openNums[i];

                        if(i<this._data.type)
                        {
                           nums+= "<color=#ff0000>"+numTemp+" "+ "</c>";
                        }
                        else
                        {
                            nums+= "<color=#7C797E>"+numTemp+" " + "</c>";
                        }    
                    }
                }
                break;
            }
            

            this.rtOpenNum.string = nums;
            this.labSum.string = this._data.sum;
            this.labSpan.string = this._data.span;
            this.labRepeat.string = this._data.repeat;

        }
    },

    /** 
    * 接收走势图开奖信息
    * @method init
    * @param {Object} data
    */
    init:function(data){
        this._data = data;

    },

    /** 
    * 刷新走势图开奖信息
    * @method updateData
    * @param {Object} data
    */
    updateData:function(data){
        if(data != null)
        {
            this._data = data;
            var isus =  this._data.Isuse.toString();
            var isuseStr = isus.substring(isus.length-2,isus.length) + "期";
            this.labIssue.string = isuseStr;
            var nums = "";

            switch (this._data.type)
            {
                case 0:
                {
                    for(var i=0;i<this._data.openNums.length;i++)
                    {
                        var numTemp = this._data.openNums[i]>9?this._data.openNums[i].toString():"0" + this._data.openNums[i];
                        nums+=numTemp+" ";
                    }
                }
                break;
                case 1:
                case 2:
                case 3:
                {
                    for(var i=0;i<this._data.openNums.length;i++)
                    {
                        var numTemp = this._data.openNums[i]>9?this._data.openNums[i].toString():"0" + this._data.openNums[i];

                        if(i<this._data.type)
                        {
                           nums+= "<color=#ff0000>"+numTemp+" "+ "</c>";
                        }
                        else
                        {
                            nums+= "<color=#7C797E>"+numTemp+" " + "</c>";
                        }    
                    }
                }
                break;
            }

            this.rtOpenNum.string = nums;
            this.labSum.string = this._data.sum;
            this.labSpan.string = this._data.span;
            this.labRepeat.string = this._data.repeat;

        }
    },

    /** 
    * 接收走势图开奖列表id
    * @method onItemIndex
    * @param {Number} i
    */
    onItemIndex: function(i){
        this._itemId = i;
    }

});
