/**
 * !#zh 双色球走势图开奖组件
 * @information 期号，红球，蓝球
 */
cc.Class({
    extends: cc.Component,

    properties: {
        labIsuse:{
            default: null,
            type: cc.Label
        },
        labRedNums:{
            default:null,
            type:cc.Label
        },
        labBlueNums:{
            default:null,
            type:cc.Label
        },
        _data:null,
        _itemId: null
    },

    // use this for initialization
    onLoad: function () {
        if(this._data != null)
        {
            this.node.color = this._data.color;
            var isuseStr = this._data.Isuse.substring(this._data.Isuse.length-3,this._data.Isuse.length) + "期";
            this.labIsuse.string = isuseStr;

            var redStr = "";
            for(var i=0;i<this._data.redNums.length;i++)
            {
                redStr += this.splitTwo(this._data.redNums[i])+" ";
            }
            this.labRedNums.string = redStr;

            var blueStr = "";
            blueStr += this.splitTwo(this._data.blueNums)+" ";
            this.labBlueNums.string = blueStr;

        }
    },

    /** 
    * 将数字转换成字符串且长度为2
    * @method splitTwo
    * @param {Number} num
    * @return {String} 长度为2的字符串
    */
    splitTwo:function(num){
        return num.toString().length<2?"0"+num:num;
    },

    /** 
    * 刷新走势图开奖信息
    * @method updateData
    * @param {Object} data
    */
    updateData:function(data){
        if(data != null)
        {
            this._data = data;
            var isuseStr = this._data.Isuse.substring(this._data.Isuse.length-3,this._data.Isuse.length) + "期";
            this.labIsuse.string = isuseStr;

            var redStr = "";
            for(var i=0;i<this._data.redNums.length;i++)
            {
                redStr += this.splitTwo(this._data.redNums[i])+" ";
            }
            this.labRedNums.string = redStr;

            var blueStr = "";
            blueStr += this.splitTwo(this._data.blueNums)+" ";
            this.labBlueNums.string = blueStr;

        }
    },

    /** 
    * 接收走势图开奖信息
    * @method init
    * @param {Object} data
    */
    init:function(data){
        this._data = data;
        
    },

    /** 
    * 接收走势图开奖列表id
    * @method onItemIndex
    * @param {Number} i
    */
    onItemIndex: function(i){
        this._itemId =i;
    }

});
