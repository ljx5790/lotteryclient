/**
 * 绑定手机号
 */
cc.Class({
    extends: cc.Component,

    properties: {
        edNumber:{
            default: null,
            type: cc.EditBox
        },

        edCode:{
            default: null,
            type: cc.EditBox
        },

        labDec:{
            default:null,
            type:cc.Label
        },

        intervalTime: 60,
        _bgetVerity:true,
        _phone:"",
        _verify:"",
    },

    // use this for initialization
    onLoad: function () {
        this.node.on(cc.Node.EventType.MOUSE_DOWN, function (event) {
             console.log('Mouse down'); 
         }, this);
         this.node.on(cc.Node.EventType.TOUCH_MOVE, function (event) {
             console.log('Mouse TOUCH_MOVE');  
         }, this);
        
        this._phone = "";
        this._verify = "";

    },

    onTextChangedVerify:function(text, editbox, customEventData){
        var txt = text;
        if(Utils.isInt(txt) || txt == "")
            this._verify = txt;
        else
        {
            txt = this._verify;
            editbox.string = txt;
            return;
        }
     },

    onTextChangedPhone:function(text, editbox, customEventData){
        var txt = text;
        if(Utils.isInt(txt) || txt == "")
            this._phone = txt;
        else
        {
            txt = this._phone;
            editbox.string = txt;
            return;
        }
    },

    checkInput:function(){
        if(this.edNumber.string != "")
        {
            if(!Utils.isPhoneNumber(this.edNumber.string))
            {
                ComponentsUtils.showTips("手机号格式不对！");
                return false;
            }
        }
        else
        {
            ComponentsUtils.showTips("手机号不能为空！");
            return false;
        }
        if(this.edCode.string != "")
        {
            if(!Utils.isVerify(this.edCode.string))
            {
                ComponentsUtils.showTips("验证码格式不对！");
                return false;
            }
        }
        else
        {
            ComponentsUtils.showTips("验证码不能为空！");
            return false;
        }
        return true;
    },

    _updateVerify: function(){
        this.intervalTime = this.intervalTime-1;
        this.labDec.string = this.intervalTime+"秒后重发";
        if(this.intervalTime <= 0){
            this.intervalTime = 60;
            this._bgetVerity = true;
            this.unschedule(this._updateVerify);
            this.labDec.string = "获取验证码"
        }
    },

    onGetVerifyCode: function(){
        if(!this._bgetVerity)
            return;
        if(this.edNumber.string == "")
        {
            ComponentsUtils.showTips("请输入手机号");
            return;
        }    

        var self = this;
        var recv = function(ret){
            ComponentsUtils.unblock();
            if (ret.Code !== 0) {
                console.log(ret.Msg);
                ComponentsUtils.showTips(ret.Msg);
            }else{
                self._bgetVerity = false;
                ComponentsUtils.showTips("发送成功!");   
                self.schedule(self._updateVerify, 1);
            }
        }.bind(this);
        cc.log("发送token"+User.getLoginToken());
        var data = {
            Equipment: Utils.getEquipment(),
            Token: User.getLoginToken(),
            Mobile: this.edNumber.string,
            VerifyType:DEFINE.SHORT_MESSAGE.CHANGEBINDPHONE,
        };
        CL.HTTP.sendRequest(DEFINE.HTTP_MESSAGE.GETVERIFY, data, recv.bind(this),"POST");  
        ComponentsUtils.block();
    },

    onTrue: function(){
        if(this.checkInput())
        {
            var recv = function(ret){
                ComponentsUtils.unblock();
                if (ret.Code !== 0) {
                    console.log(ret.Msg);
                    ComponentsUtils.showTips(ret.Msg);
                }else{
                    User.setTel(this.edNumber.string);
                    //User.setIsbindtel(true);
                    window.Notification.emit("USER_informationrefresh","");
                    window.Notification.emit("USER_useruirefresh","");
                    //ComponentsUtils.showTips("修改成功");         
                    this.onClose();
                }
            }.bind(this);

            var data = {
                Token: User.getLoginToken(),
                UserCode:User.getUserCode(),
                Mobile: this.edNumber.string,
                VerifyCode: this.edCode.string,
            };
            CL.HTTP.sendRequestRet(DEFINE.HTTP_MESSAGE.CHANGEPHONENUMBER, data, recv.bind(this),"POST");  
            ComponentsUtils.block();
        }
    },

    resetRefresh:function(){
        this.edNumber.string = "";
        this.edCode.string = "";
        this._bgetVerity = true;
        this.unschedule(this._updateVerify);
    },

    onClose:function(){
        this.node.getComponent("Page").backAndRemove();
    },
});
