/*
用户信息界面
*/ 
cc.Class({
    extends: cc.Component,

    properties: {
        labName:{
            default: null,
            type: cc.Label
        },

        labBind:{
            default: null,
            type: cc.Label
        },

        labRealName:{
            default: null,
            type: cc.Label
        },

        btnRealName:{
            default: null,
            type: cc.Button
        },

        modifyPhoneNumPrefab: {
            default: null,
            type: cc.Prefab
        },

        bindPhoneNumPrefab: {
            default: null,
            type: cc.Prefab
        },

        verifiedPrefab: {
            default: null,
            type: cc.Prefab
        },

        bindBankPrefab: {
            default: null,
            type: cc.Prefab
        },

        modifyPwdPrefab:{
            default: null,
            type: cc.Prefab
        },

        //修改昵称page
        modifyNamePrefab:{
            default: null,
            type: cc.Prefab
        },

        ndContent:{
            default: null,
            type: cc.Node
        }

    },

    // use this for initialization
    onLoad: function () {
    //    cc.log("onload:[user_information_pop]");
        
        window.Notification.on("USER_informationrefresh",function(data){
        //    cc.log("refreshUserUI-information");
            this.refreshInfo();
        },this);

         this.node.on(cc.Node.EventType.TOUCH_END, function (event) {
                var contentRect = this.ndContent.getBoundingBoxToWorld();
                var touchLocation = event.getLocation();
                if(cc.rectContainsPoint(contentRect, touchLocation) == false){
                    this.onClose();
                }
        }, this);

        this.refreshInfo();
    },

    refreshInfo:function(){
        var tel = User.getTel();
        if(tel != "")
        {
            var top = "*******";
            var down = tel.substring(7,11);
            this.labBind.string = top+down;
        }
        else
        {
            this.labBind.string = "未绑定";
        }

        this.labName.string = User.getNickName()==""?"昵称":User.getNickName();
        this.labRealName.string = (User.getFullName()=="" || User.getFullName()==" ")?"姓名未认证":User.getFullName();
        this.btnRealName.node.getChildByName("spSelected").active = User.getIsCertification()?true:false;

    },

    //修改昵称
    onNameModifyBtn:function(){
        var canvas = cc.find("Canvas");
        var modifyName = cc.instantiate(this.modifyNamePrefab);
        canvas.addChild(modifyName);
    },

    //更换手机号码
    onPhoneModifyBtn:function(){   
        var canvas = cc.find("Canvas");
        if(User.getIsbindtel())
        { 
            var modifyPhoneNumPrefab = cc.instantiate(this.modifyPhoneNumPrefab);
            canvas.addChild(modifyPhoneNumPrefab);
            modifyPhoneNumPrefab.active = true;
        }
        else
         {
            var canvas = cc.find("Canvas");
            var bindPhoneNum = cc.instantiate(this.bindPhoneNumPrefab);
            canvas.addChild(bindPhoneNum);
            bindPhoneNum.active = true;
        }
    },

    //实名认证
    onRealNameBtn:function(){
        if(!User.getIsCertification())
        {
             var canvas = cc.find("Canvas");
            var verifiedPrefab = cc.instantiate(this.verifiedPrefab);
            canvas.addChild(verifiedPrefab);
            verifiedPrefab.active = true;

        }
    },

    //添加银行卡
    onBankCardBtn:function(){
        var canvas = cc.find("Canvas");
        var bindBankPrefab = cc.instantiate(this.bindBankPrefab);
        bindBankPrefab.getComponent("user_bankCard_pop").init(2,null);
        canvas.addChild(bindBankPrefab);
        bindBankPrefab.active = true;
    },

    //修改提现密码
    onCashPasswordBtn:function(){
        if(User.getIsbindtel())
        {
            var canvas = cc.find("Canvas");
            var modifyPwdPrefab = cc.instantiate(this.modifyPwdPrefab);
            modifyPwdPrefab.getComponent("user_modifyCashPwd_pop").init(2);
            canvas.addChild(modifyPwdPrefab);
            modifyPwdPrefab.active = true;
        }
        else
        {
            var canvas = cc.find("Canvas");
            var verifiedPrefab = cc.instantiate(this.verifiedPrefab);
            canvas.addChild(verifiedPrefab);
            verifiedPrefab.active = true;
        }
    },

    //修改登录密码
    onLoginPasswordBtn:function(){
        if(User.getIsbindtel())
        {
            var canvas = cc.find("Canvas");
            var modifyPwdPrefab = cc.instantiate(this.modifyPwdPrefab);
            modifyPwdPrefab.getComponent("user_modifyCashPwd_pop").init(1);
            canvas.addChild(modifyPwdPrefab);
            modifyPwdPrefab.active = true;
            this.onClose();
        }
        else
        {
            var canvas = cc.find("Canvas");
            var verifiedPrefab = cc.instantiate(this.verifiedPrefab);
            canvas.addChild(verifiedPrefab);
            verifiedPrefab.active = true;
        }
    },

    onClose:function(){
        window.Notification.offType("USER_informationrefresh");
        this.node.getComponent("Page").backAndRemove();
    },

    

});
