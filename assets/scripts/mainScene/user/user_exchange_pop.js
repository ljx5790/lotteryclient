/**
 * 充值兑换中心界面
 */
cc.Class({
    extends: cc.Component,

    properties: {
        labName: {
            default: null,
            type: cc.Label
        },

        labBalance: {
            default: null,
            type: cc.Label
        },

        labGold: {
            default: null,
            type: cc.Label
        },

        labCurGold:{
            default: null,
            type: cc.Label
        },

        labPayMoney:{
            default: null,
            type: cc.Label
        },

        ndrecharge:cc.Node,
        ndPay:cc.Node,

        _needPayMoney:0,
        _goldNums:0,
        _retCallBack: null,
    },

    // use this for initialization
    onLoad: function () {
        this._needPayMoney = 10;
        this._goldNums = 10000;

        this.labCurGold.string = User.getGoldBean()+"个";
        this.labGold.string = "10000个";
        this.labName.string = User.getNickName();
        this.labBalance.string = User.getBalance().toString() + "元";

        if(User.getBalance()>=10)
        {
           this.labPayMoney.string = "10元";
           this.ndPay.active = true;
        }
        else
        {
            this.ndrecharge.active = true;
        }
    },

    init:function(callback){
        this._retCallBack = callback;
    },

    onSelectMoneyBtn: function(toggle, customEventData){
        this._needPayMoney = customEventData;
        this._goldNums = customEventData*1000;

        this.labGold.string = this._goldNums.toString() + "个";

        if(User.getBalance()>=customEventData)
        {
            this.labPayMoney.string = customEventData+"元";
            this.ndPay.active = true;
        }
        else
        {
            this.ndPay.active = true;
        }
    },

    //微信支付
    onPayWeiChat:function(){
        ComponentsUtils.showTips("暂时不支持微信支付！");
    },

    //支付宝支付
    onAliPay: function()
    {
        ComponentsUtils.showTips("暂时不支持支付宝支付！");
    },
        
    onClose: function(){
        this.node.getComponent("Page").backAndRemove();
    },

    onPay:function(){
        var recv = function(ret){
            ComponentsUtils.unblock();
            if (ret.Code !== 0) {
                console.log(ret.Msg);
                ComponentsUtils.showTips(ret.Msg);
            }else{
                ComponentsUtils.showTips(ret.Msg);
                User.setBalance(ret.Balance);
                User.setGoldBean(ret.Gold);
                this._retCallBack();
                this.onClose();
            }
        }.bind(this);

        var data = {
            Token: User.getLoginToken(),
            UserCode:  User.getUserCode(),
            Amount: LotteryUtils.moneytoClient(this._needPayMoney),
            Gold:this._goldNums,
        };
        CL.HTTP.sendRequest(DEFINE.HTTP_MESSAGE.EXCHANGEBEAN, data, recv.bind(this),"POST");  
        ComponentsUtils.block();
    },

      //刷新用户信息
      updateUser: function(){
        var recv = function(ret){
            if(ret.Code === 0)
            {
                User.setBalance(ret.Balance);
                User.setGoldBean(ret.Gold);
            }
            else
            {
                ComponentsUtils.showTips(ret.Msg);
            }
        }.bind(this);
        var data = {
            Token:User.getLoginToken(),
            UserCode:User.getUserCode(),
        }
        CL.HTTP.sendRequest(DEFINE.HTTP_MESSAGE.GETUSERMONEY, data, recv.bind(this),"POST");  
    },

});
