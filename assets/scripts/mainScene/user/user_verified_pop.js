/**
 * 实名认证界面
 */
cc.Class({
    extends: cc.Component,

    properties: {
        edName:{
            default: null,
            type: cc.EditBox
        },

        edNumber:{
            default: null,
            type: cc.EditBox
        },

        edPhoneNumber:{
            default: null,
            type: cc.EditBox
        },

        edVerifCode:{
            default: null,
            type: cc.EditBox
        },

        labTel:{
            default: null,
            type: cc.Label
        },

        labDec:{
            default:null,
            type:cc.Label
        },

        _tel:"",
        _bgetVerity:true,
        intervalTime: 60
    },

    // use this for initialization
    onLoad: function () {
        this._tel = User.getTel();
        if(this._tel != "")
        {
            var top = "*******";
            var down = this._tel.substring(7,11);
            this.edPhoneNumber.node.active = false;
            this.labTel.string = top+down;
        }
        else
        {
            this._tel= "";
        }   
    },

    _updateVerify: function(){
        this.intervalTime = this.intervalTime-1;
        this.labDec.string = this.intervalTime+"秒后重发";
        if(this.intervalTime <= 0){
            this.intervalTime = 60;
            this._bgetVerity = true;
            this.unschedule(this._updateVerify);
            this.labDec.string = "获取验证码";
        }
    },

    onTextChangedVerify:function(text, editbox, customEventData){
        var txt = text;
        if(Utils.isInt(txt) || txt == "")
            this.verify = txt;
        else
        {
            txt = this.verify;
            editbox.string = txt;
            return;
        }
     },

    onTextChangedPhone:function(text, editbox, customEventData){
        var txt = text;
        if(Utils.isInt(txt) || txt == "")
            this.phoneNum = txt;
        else
        {
            txt = this.phoneNum;
            editbox.string = txt;
            return;
        }
     },

    onTextChangedNumber:function(text, editbox, customEventData){
        var txt = text;
        if(Utils.isInt(txt) || txt == "")
            this.num = txt;
        else
        {
            txt = this.num;
            editbox.string = txt;
            return;
        }
     },

    onGetVerifyCode: function(){
        if(!this._bgetVerity)
            return;
        if(!this.checkInput(false))
            return;
  
        var self = this;
        var recv = function(ret){
            ComponentsUtils.unblock();
            if (ret.Code !== 0) {
                console.log(ret.Msg);
                ComponentsUtils.showTips(ret.Msg);
            }else{
                self._bgetVerity = false;
                ComponentsUtils.showTips("发送成功!");   
                self.schedule(self._updateVerify, 1);
            }
        }.bind(this);
        cc.log("发送token"+User.getLoginToken());
        var data = {
            Equipment: Utils.getEquipment(),
            Token: User.getLoginToken(),
            Mobile: this._tel == "" ? this.edPhoneNumber.string : this._tel,
            VerifyType:DEFINE.SHORT_MESSAGE.IDENTVEIFY,
        };
        CL.HTTP.sendRequest(DEFINE.HTTP_MESSAGE.GETVERIFY, data, recv.bind(this),"POST");  
        ComponentsUtils.block();
    },

    checkInput:function(type){

        if(this.edName.string != "")
        {
            if(!Utils.isChineseChar(this.edName.string))
            {
                ComponentsUtils.showTips("名字格式错误！");
                return false;
            }
        }
        else
        {
            ComponentsUtils.showTips("名字不能为空！");
            return false;
        }
        if(this.edNumber.string != "")
        {
            if(!Utils.isValidateIdCard(this.edNumber.string))
            {
                return false;
            }
        }
        else
        {
            ComponentsUtils.showTips("身份证不能为空！");
            return false;
        }
        var tel = this._tel==""?this.edPhoneNumber.string:this._tel;
        if(tel != "")
        {
            if(!Utils.isPhoneNumber(tel))
            {
                ComponentsUtils.showTips("手机号格式不对！");
                return false;
            }
        }
        else
        {
            ComponentsUtils.showTips("手机号不能为空！");
            return false;
        }
        if(type)
        {
            if(this.edVerifCode.string != "")
            {
                if(!Utils.isVerify(this.edVerifCode.string))
                {
                    ComponentsUtils.showTips("验证码格式不对！");
                    return false;
                }
            }
            else
            {
                ComponentsUtils.showTips("验证码不能为空！");
                return false;
            }
        }

        return true;
    },

    onTrue: function(){
        if(!this.checkInput(true))
            return;
        var recv = function(ret){
            ComponentsUtils.unblock();
            if(ret.Code == 0)
            {
                ComponentsUtils.showTips("认证成功！");
                User.setFullName(this.edName.string);
                User.setIsCertification(true);
                //User.setIsbindtel(true);
                User.setTel(this._tel);
                window.Notification.emit("USER_informationrefresh","");
                window.Notification.emit("USER_useruirefresh","");
                this.onClose();
            }
            else
            {
                ComponentsUtils.showTips(ret.Msg);
            }
        }.bind(this);

        var data = {
            Token: User.getLoginToken(),
            UserCode: User.getUserCode(),
            FullName: this.edName.string,
            IDCardNo: this.edNumber.string,
            VerifyCode: this.edVerifCode.string,
            Mobile:this._tel == "" ? this.edPhoneNumber.string : this._tel,
        };
        CL.HTTP.sendRequestRet(DEFINE.HTTP_MESSAGE.IDENTITYAUTHENTICATION, data, recv.bind(this),"POST");  
        ComponentsUtils.block();
    },

    onClose: function(){
         this.node.getComponent("Page").backAndRemove();
    }

});
