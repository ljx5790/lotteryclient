
/*聊天室彩票信息面板 */
cc.Class({
    extends: cc.Component,

    properties: {
        rightRewardInfo:{
            default:null,
            type:cc.Node
        },
        k3Reward:{
            default: null,
            type: cc.Prefab
        },
        X5Reward:{
            default: null,
            type: cc.Prefab
        },
        duotoneBallReward:{
            default: null,
            type: cc.Prefab
        },
        bigLottoReward:{
            default: null,
            type: cc.Prefab
        },
        publicRewardTop:{
            default: null,
            type: cc.Prefab
        },
        k3RewardTop:{
            default: null,
            type: cc.Prefab
        },
        isuseContent:{
            default: null,
            type: cc.Layout
        },
        rewardHistoryIssue:{
            default:null,
            type:cc.Node
        },

        spHistoryArrow:{
            default:null,
            type:cc.Sprite
        },

        _curRewardInfo:null,//当前记录  
        _isuseNum:0,   //当前期号
        _endTimeStamp:0,//结束时间
        _totalisuse:0,//一天总期数
        _curServerTime:0,//实时服务器时间
        _lotteryId:"",//彩种id
 
        _isWaitOpen:false,//是否等待开奖中
        _isUpdating:false,//刷新历史记录中
        _isSynchrTime:true,//是否同步服务器时间
        _isPre:false,//是否预售

        _openList:[],//开奖列表
        _fOpenList:[],//首个开奖信息
        _hpConState:false,
    },

    // use this for initialization
    onLoad: function () {
        
        //初始化开奖信息栏
        this._curRewardInfo = null;
        if(this._lotteryId == "101" || this._lotteryId == "102"){
            this._curRewardInfo = this.node.getChildByName("TopInfoContent").getChildByName("K3LeftRewardInfo");
        }else if(this._lotteryId == "201" || this._lotteryId == "202"){
            this._curRewardInfo = this.node.getChildByName("TopInfoContent").getChildByName("HB11X5LeftRewardInfo");  
        }else if(this._lotteryId == "301"){
            this._curRewardInfo = this.node.getChildByName("TopInfoContent").getChildByName("CQSSCLeftRewardInfo");   
        }else if(this._lotteryId == "801"){
            this._curRewardInfo = this.node.getChildByName("TopInfoContent").getChildByName("DCBLeftRewardInfo");
        }else if(this._lotteryId == "901"){
            this._curRewardInfo = this.node.getChildByName("TopInfoContent").getChildByName("BigLottoRewardInfo");
        }

        if(this._curRewardInfo){
            this._curRewardInfo.active = true;
            this.spHistoryArrow.node.active = true;
        }

        this.startTime();
        //当期信息
        this.getCurrentIsuse();
       
        window.Notification.on('rOpen_push',function(data){
            this.refreshOpen(data.data); 
        }.bind(this));
    },

    initBetControl:function(lotteryId){
        this._lotteryId = lotteryId.toString();      
        this._isSynchrTime = true;
    },

    //倒计时显示刷新
    betUpdater:function(dt){
        if( this._curServerTime == 0)
            return;
        var currentTimeStamp = this._curServerTime;
        var time = 0;
        var leftTimeStamp = this._endTimeStamp - currentTimeStamp;
        
        if(leftTimeStamp >= 0){
            time = this._endTimeStamp - currentTimeStamp;
        }
        else
        {
            this.rightRewardInfo.getChildByName("TimeLabel").getComponent(cc.Label).string = "--:--:--"; 
            this.unschedule(this.betUpdater);
            this.refreshShow();
            return;
        }

        var h = parseInt(time/(60*60)).toString();
        var hStr = (parseInt(h)%24).toString();
        var dStr = parseInt( parseInt(h)/24).toString();
        hStr = hStr.length==1?("0"+hStr):hStr;
        var m = (parseInt((time-h*60*60)/60)).toString();
        m = m.length==1?("0"+m):m;
        var s = (parseInt(time-h*60*60-m*60)).toString();
        s = s.length==1?("0"+s):s;
        var timeStr = "";
        if(parseInt(dStr)>0){
            timeStr = dStr+"天 "+ hStr+":"+m+":"+s;
        }else{
            timeStr = hStr+":"+m+":"+ s;
        }
        this.rightRewardInfo.getChildByName("TimeLabel").getComponent(cc.Label).string = timeStr;
    },
    
    //刷新显示
    refreshShow: function(){
        this._isSynchrTime = true; 
        var currentTimeStamp = this._curServerTime;
        var leftTimeStamp = this._endTimeStamp - currentTimeStamp;
        if(leftTimeStamp > 0){
        this.schedule(this.betUpdater, 1);
        }else{
            this.getCurrentIsuse();
            this.getIsuseList(this._lotteryId, "1", "11");
        }
    },

    //得到上一期期数
    getLastIsuseNum:function(cur,len){
        if(this._isuseNum == 0 || cur == 0)
            return "0";
        var lastNum = cur-1;
        var lastStr = lastNum.toString();
        var leftnum = this._isuseNum.substr(0,this._isuseNum.length-len);
        var rightnum = lastStr.length>=len?lastStr:"0"+lastStr;
        return leftnum+rightnum;
    },

    //获取当期是否截止
    getCurrentIsuseAvailable:function(){
        var currentTimeStamp = this._curServerTime;
        var leftTimeStamp = this._endTimeStamp - currentTimeStamp;
        
        if(leftTimeStamp <= 0){
            return false;
        }else{
            return true;
        }
    },

    //时间刷新--
    startTime: function()
    {
        this.updateServerT();
        this.schedule(this.updateServerT, 10);
        this.schedule(this.updateTime, 1);
        
    },

    getConnetState:function(){
        return this._hpConState;
    },

    updateServerT:function()
    {
        if(CL.NET.isConnect())
        {
            this._hpConState = false;
            this._curServerTime = CL.NET.getServerTime()/1000;
        }
        else
        {
            this.curServerTime = CL.MANAGECENTER.curServerTime;
        }
    },

    updateTime :function()
    {
        if(this._curServerTime == 0 )
            return;
        this._curServerTime += 1;
    },

    getCurTime:function()
    {
        return this._curServerTime;
    },

    //获取当期信息
    getCurrentIsuse:function(){
        this._isPre = false;
        var recv = function(ret){
           
            var obj = ret.Data;
            if(ret.Code == 49)
            {
                this.getCurrentIsuse();
                return;
            }
            if(ret.Code === 0){
              
                this._isuseNum = obj.IsuseNum;  
                var endTimeDate = Utils.getDateForStringDate(obj.EndTime.toString());  
                this._endTimeStamp = Date.parse(endTimeDate)/1000;
                this._totalisuse = parseInt(obj.TotalIsuse);
                var titleLabelStr = "<color=#FFFFFF>距</color><color=#FFE956>"+this._isuseNum.toString()+"</color><color=#FFFFFF>期截止</color>";
                this.rightRewardInfo.getChildByName("TitleLabel").getComponent(cc.RichText).string = titleLabelStr;     
                this.schedule(this.betUpdater, 1);   
                this.rightRewardInfo.getChildByName("TitleLabel").active = true;
                this.rightRewardInfo.getChildByName("TimeLabel").active = true;
                this.rightRewardInfo.getChildByName("labNullL").active = false;
                TrentChart.setEndTime(this._endTimeStamp);
                TrentChart.setCurIssue(obj.IsuseNum);
                
            }else{
                if(ret.Code == 90 || ret.Code == 91)
                {
                    this._isPre = ret.Code == 90?true:false;
                    TrentChart.setEndTime(ret.Code);
                    TrentChart.setCurIssue(obj.IsuseNum);
                    this._isuseNum = obj.IsuseNum;
                    var titleLabelStr = "<color=#FFFFFF>第</color><color=#FFE956>"+this._isuseNum.toString()+"</color><color=#FFFFFF>期</color>";
                    this.rightRewardInfo.getChildByName("TitleLabel").getComponent(cc.RichText).string = titleLabelStr;   
                    this.rightRewardInfo.getChildByName("TimeLabel").getComponent(cc.Label).string = ret.Msg;
                    this.rightRewardInfo.getChildByName("labNullL").active = false;
                    this.rightRewardInfo.getChildByName("TitleLabel").active = true;
                    this.rightRewardInfo.getChildByName("TimeLabel").active = true;
                }    
                else 
                {
                    TrentChart.setEndTime("");
                    TrentChart.setCurIssue("");
                    this.rightRewardInfo.getChildByName("TitleLabel").active = false;
                    this.rightRewardInfo.getChildByName("TimeLabel").active = false;
                    this.rightRewardInfo.getChildByName("labNullL").active = true;
                }
                cc.error(ret.Msg);
            }
            //历史期数
            this.getIsuseList(this._lotteryId, "1", "11");
        }.bind(this);

        var data = {
            Token:User.getLoginToken(),
            LotteryCode:this._lotteryId,
        }
        CL.HTTP.sendRequestRet(DEFINE.HTTP_MESSAGE.GETRECURRENTISUSE, data, recv.bind(this),"POST");   
    },
    
    //展开列表
    onShowIsuseList:function(){
        if(this.rightRewardInfo.getChildByName("labNullR").active == false)
        {
            this.rewardHistoryIssue.active = true;
            this.rewardHistoryIssue.emit('isuse-in');
            this.spHistoryArrow.node.rotation =  0;
        }    
    },

    //收回列表
    onHideIsuseList:function(){ 
        this.rewardHistoryIssue.emit('isuse-out');
       this.spHistoryArrow.node.rotation =  180;
    },

    //添加和刷新开奖列表
    addIsuseContent:function(data,curPre){
        if(this.isuseContent.node.childrenCount == 0)
        {
            var top = cc.instantiate(this.publicRewardTop);
            this.isuseContent.node.addChild(top);
        }

        this.rightRewardInfo.getChildByName("labNullR").active = false;
        var isWaitOpenTemp = false;
        for(var i = 0;i<data.length;i++)
        {
            var spriteNums = [];
            var petNumberArray = [];
            if( data[i].Number != "")
            {
                petNumberArray = data[i].Number.split(" ");
                for(var j=0;j<petNumberArray.length;j++)
                {
                    spriteNums.push(petNumberArray[j]); 
                }
            }

            if(i == 0)
            {
                this._fOpenList = data[0];
                var lens = 3;
                var isuseStr = data[i].IsuseNum;
                var isuseTemp = isuseStr.substr(isuseStr.length-lens);
                var isuseInt = parseInt(isuseTemp);
                var curInt = 0;
                if(this._isuseNum != 0)
                {
                    var curStr = this._isuseNum;
                    var curTemp = curStr.substr(curStr.length-lens);
                    curInt = parseInt(curTemp);
                }
                
                if(curInt-1  == isuseInt || this._isPre)//当前期号与列表期号相差1
                {  
                    this._curRewardInfo.getChildByName("IsuseLabel").getComponent(cc.Label).string = "第"+data[i].IsuseNum+"期开奖";
                    this._curRewardInfo.getChildByName("IsuseLabel").active = true;
                    this._curRewardInfo.getChildByName("RewardNumLabel").active = false;
                    var diceNode = this._curRewardInfo.getChildByName("LuckyBallContent");
                    diceNode.active = true;
                    if(spriteNums.length >0)
                    {
                        for(var k=0;k<spriteNums.length;k++)
                        {
                            var cout = k+1;
                            var ballname = "ball" + cout.toString();
                            try {
                                if(data[i].LotteryCode == 301){
                                    diceNode.getChildByName(ballname).getChildByName("label").getComponent(cc.Label).string = spriteNums[k];
                                }
                                else{
                                    diceNode.getChildByName(ballname).getChildByName("label").getComponent(cc.Label).string = (spriteNums[k]=="0")?"?":spriteNums[k];  
                                }
                            } catch (error) {
                                
                            }
                        }        
                    }
                    isWaitOpenTemp = false; 
                    continue;
                }
                else
                {
                    var lastIsuse = this.getLastIsuseNum(curInt,lens);
                    this._curRewardInfo.getChildByName("IsuseLabel").getComponent(cc.Label).string = "第"+lastIsuse+"期开奖";
                    this._curRewardInfo.getChildByName("IsuseLabel").active = true;
                    this._curRewardInfo.getChildByName("RewardNumLabel").active = true;
                    this._curRewardInfo.getChildByName("LuckyBallContent").active = false;
                    isWaitOpenTemp = true;
                }
            }
            if(this.isuseContent.node.childrenCount>data.length)
            {
                var children = this.isuseContent.node.children;
                children.getComponent('chat_public_reward').init({
                    nums: spriteNums,
                    isuse: data[i].IsuseNum,
                });
            }
            else
            {
                var reward = cc.instantiate(curPre);
                reward.getComponent('chat_public_reward').init({
                    nums: spriteNums,
                    isuse: data[i].IsuseNum,
                });
                this.isuseContent.node.addChild(reward);
            }
        }
        this._isWaitOpen = isWaitOpenTemp;
    },

    //刷新开奖列表信息
    refreshOpen:function(data){
        if(data.LotteryCode == this._lotteryId)
        {
            if(this._openList.length>0)
            {
                this._openList.unshift(data);
                this._openList.pop();
            }
            else
            {
                this._openList.push(data);
            }
            this.resetIsuseList(this._openList);
        }
    },

    resetIsuseList:function(data){
        this.rightRewardInfo.getChildByName("labNullR").active = false;
        this.isuseContent.node.removeAllChildren();
        var data = data;
        var ID = parseInt(this._lotteryId);

        switch (ID)
        {
            case 101: //k3
            case 102:
            {
                if(this.isuseContent.node.childrenCount == 0)
                {   
                    var top = cc.instantiate(this.k3RewardTop);
                    this.isuseContent.node.addChild(top);
                }
                var isWaitOpenTemp = false;
                for(var i = 0;i<data.length;i++)
                {
                    var spriteNums = [];
                    var sumValue = 0;
                    var stateStr = "";
                    var petNumberArray = [];
                    if(data[i].Number != "" )
                    {
                        petNumberArray = data[i].Number.split(" ");
                        for(var j=0;j<petNumberArray.length;j++)
                        {
                           spriteNums.push(CL.MANAGECENTER.getDiceSpriteFrameByNum(petNumberArray[j])); 
                           var value = parseInt(petNumberArray[j]);
                           sumValue += value;
                        }
                    }
                    
                    if(i == 0)
                    {
                        var lens = this._totalisuse.toString().length;
                        var isuseStr = data[i].IsuseNum;
                        var isuseTemp = isuseStr.substr(isuseStr.length-lens);
                        var isuseInt = parseInt(isuseTemp);
                        var curInt = 0;
                        
                        if(this._isuseNum != 0)
                        {
                            var curStr = this._isuseNum;
                            var curTemp = curStr.substr(curStr.length-lens);
                            curInt = parseInt(curTemp);
                        }
                      
                        if(curInt-1  == isuseInt || this._isPre)//当前期号与列表期号相差1
                        {
                            isWaitOpenTemp = false;
                            this._curRewardInfo.getChildByName("RewardNumLabel").active = false;
                            this._curRewardInfo.getChildByName("IsuseLabel").getComponent(cc.Label).string = "第"+data[i].IsuseNum+"期开奖";
                            this._curRewardInfo.getChildByName("IsuseLabel").active = true;
                            var diceNode = this._curRewardInfo.getChildByName("DiceNode");
                            diceNode.active = true;
                            if(spriteNums.length == 3)
                            {
                                diceNode.getChildByName("Dice_1").getComponent(cc.Sprite).spriteFrame = spriteNums[0];
                                diceNode.getChildByName("Dice_2").getComponent(cc.Sprite).spriteFrame = spriteNums[1];
                                diceNode.getChildByName("Dice_3").getComponent(cc.Sprite).spriteFrame = spriteNums[2];
                            }
                            continue;
                        }
                        else
                        {
                            isWaitOpenTemp = true;
                            var lastIsuse = this.getLastIsuseNum(curInt,lens);
                            this._curRewardInfo.getChildByName("IsuseLabel").getComponent(cc.Label).string = "第"+lastIsuse+"期开奖";
                            this._curRewardInfo.getChildByName("IsuseLabel").active = true;
                            var diceNode = this._curRewardInfo.getChildByName("DiceNode");
                            diceNode.active = false;
                            this._curRewardInfo.getChildByName("RewardNumLabel").active = true;
                        }
                    }
                    sumValue = isNaN(sumValue)==true?0:sumValue;
                    if(sumValue != 0){
                        if(sumValue<=10){
                            stateStr = "小";
                        }else{
                            stateStr = "大";
                        }
                        if(sumValue%2 == 0){
                            stateStr += "双";
                        }else{
                            stateStr += "单";
                        }   
                    }
                    if(this.isuseContent.node.childrenCount>data.length)
                    {
                        var children = this.isuseContent.node.children;
                        for (var i = 0; i < children.length; ++i) {
                            children[i].getComponent('chat_k3Reward').init({
                                nums: spriteNums,
                                isuse: data[i].IsuseNum,
                                state: stateStr
                            });
                        }
                    }
                    else
                    {
                        var reward = cc.instantiate(this.k3Reward);
                        reward.getComponent('chat_k3Reward').init({
                            nums: spriteNums,
                            isuse: data[i].IsuseNum,
                            state: stateStr
                        });
                        this.isuseContent.node.addChild(reward);
                    }
                }
                this._isWaitOpen = isWaitOpenTemp;
            }
            break;
            case 201://115
            case 202:
            {
                this.addIsuseContent(data,this.X5Reward);
            }
            break;
            case 801://ssq
            {
               this.addIsuseContent(data,this.duotoneBallReward);
            }
            break;
            case 901://big
            {
                this.addIsuseContent(data,this.bigLottoReward);
            }
            break;
            case 301://ssc
            {
                this.addIsuseContent(data,this.X5Reward);
            }
            break;
            default:
            break;
        }
        this._isUpdating = false;
    },


    //获取开奖列表
    getIsuseList:function(lotteryid, pageindex, pagesize){
       
        if(this._isUpdating == true)
            return;
        this._isUpdating = true;
        var recv = function(ret){
            if(ret.Code !== 0)
            {   
                this.rightRewardInfo.getChildByName("labNullR").active = true;
                this._curRewardInfo.getChildByName("RewardNumLabel").active = false;
                cc.log(ret.Msg);
                this._isUpdating = false;
                
            }
            else
            {
                this.rightRewardInfo.getChildByName("labNullR").active = false;
                this.isuseContent.node.removeAllChildren();

                var data = ret.Data;
                this._openList = data.slice();
                var ID = parseInt(lotteryid);
                switch (ID)
                {
                    case 101: //k3
                    case 102:
                    {
                        if(this.isuseContent.node.childrenCount == 0)
                        {   
                            var top = cc.instantiate(this.k3RewardTop);
                            this.isuseContent.node.addChild(top);
                        }
                        var isWaitOpenTemp = false;
                        for(var i = 0;i<data.length;i++)
                        {
                            var spriteNums = [];
                            var sumValue = 0;
                            var stateStr = "";
                            var petNumberArray =[];
                            if(data[i].Number != "")
                            {
                                petNumberArray = data[i].Number.split(" ");
                                for(var j=0;j<petNumberArray.length;j++)
                                {
                                   spriteNums.push(CL.MANAGECENTER.getDiceSpriteFrameByNum(petNumberArray[j])); 
                                   var value = parseInt(petNumberArray[j]);
                                   sumValue += value;
                                }
                            }
                            if(i == 0)
                            {
                                var lens = this._totalisuse.toString().length;
                                var isuseStr = data[i].IsuseNum;
                                var isuseTemp = isuseStr.substr(isuseStr.length-lens);
                                var isuseInt = parseInt(isuseTemp);
                                var curInt = 0;
                                
                                if(this._isuseNum != 0)
                                {
                                    var curStr = this._isuseNum;
                                    var curTemp = curStr.substr(curStr.length-lens);
                                    curInt = parseInt(curTemp);
                                }
                              
                                if(curInt-1  == isuseInt || this._isPre)//当前期号与列表期号相差1
                                {
                                    isWaitOpenTemp = false;
                                    this._curRewardInfo.getChildByName("RewardNumLabel").active = false;
                                    this._curRewardInfo.getChildByName("IsuseLabel").getComponent(cc.Label).string = "第"+data[i].IsuseNum+"期开奖";
                                    this._curRewardInfo.getChildByName("IsuseLabel").active = true;
                                    var diceNode = this._curRewardInfo.getChildByName("DiceNode");
                                    diceNode.active = true;
                                    if(spriteNums.length == 3)
                                    {
                                    
                                        diceNode.getChildByName("Dice_1").getComponent(cc.Sprite).spriteFrame = spriteNums[0];
                                        diceNode.getChildByName("Dice_2").getComponent(cc.Sprite).spriteFrame = spriteNums[1];
                                        diceNode.getChildByName("Dice_3").getComponent(cc.Sprite).spriteFrame = spriteNums[2];
                                        
                                    }
                                    continue;
                                }
                                else
                                {
                                    isWaitOpenTemp = true;
                                    var lastIsuse = this.getLastIsuseNum(curInt,lens);
                                    this._curRewardInfo.getChildByName("IsuseLabel").getComponent(cc.Label).string = "第"+lastIsuse+"期开奖";
                                    this._curRewardInfo.getChildByName("IsuseLabel").active = true;
                                    var diceNode = this._curRewardInfo.getChildByName("DiceNode");
                                    diceNode.active = false;
                                    this._curRewardInfo.getChildByName("RewardNumLabel").active = true;
                                }
                            }
                            sumValue = isNaN(sumValue)==true?0:sumValue;
                            if(sumValue != 0){
                                if(sumValue<=10){
                                    stateStr = "小";
                                }else{
                                    stateStr = "大";
                                }
                                if(sumValue%2 == 0){
                                    stateStr += "双";
                                }else{
                                    stateStr += "单";
                                }   
                            }
                            if(this.isuseContent.node.childrenCount>data.length)
                            {
                                var children = this.isuseContent.node.children;
                                for (var i = 0; i < children.length; ++i) {
                                    children[i].getComponent('chat_k3Reward').init({
                                        nums: spriteNums,
                                        isuse: data[i].IsuseNum,
                                        state: stateStr
                                    });
                                }
                            }
                            else
                            {
                                var reward = cc.instantiate(this.k3Reward);
                                reward.getComponent('chat_k3Reward').init({
                                    nums: spriteNums,
                                    isuse: data[i].IsuseNum,
                                    state: stateStr
                                });
                                this.isuseContent.node.addChild(reward);
                            }
                        }
                        this._isWaitOpen = isWaitOpenTemp;
                    }
                    break;
                    case 201://115
                    case 202:
                    {
                        this.addIsuseContent(data,this.X5Reward);
                    }
                    break;
                    case 801://ssq
                    {
                       this.addIsuseContent(data,this.duotoneBallReward);
                    }
                    break;
                    case 901://big
                    {
                        this.addIsuseContent(data,this.bigLottoReward);
                    }
                    break;
                    case 301://ssc
                    {
                        this.addIsuseContent(data,this.X5Reward);
                    }
                    break;
                    default:
                    break;
                }
                this._isUpdating = false;
            }
        }.bind(this);          

        var data = {
            Token:User.getLoginToken(),
            LotteryCode:lotteryid,
            PageNumber:pageindex,
            RowsPerPage:pagesize,
        }
        CL.HTTP.sendRequestRet(DEFINE.HTTP_MESSAGE.GETTHELOTTERYLIST, data, recv.bind(this),"POST");     
    },

    onDestroy: function(){
        this.unschedule(this.updateServerT);
        this.unschedule(this.updateTime);
        window.Notification.offType("rOpen_push");
    }

});
