/*
新闻界面
*/ 
cc.Class({
    extends: cc.Component,

    properties: {
        newsPaperPrefab: cc.Prefab,  //新闻标题
        webView: cc.WebView,
        labHead: cc.Label, //新闻标题
        labTimeNameNum: cc.Label, //新闻时间 作者 阅读量
        labRed: cc.RichText,   //红球号
        labBlue: cc.RichText,  //蓝球号
        labReasonNum: cc.Label, //支持数
        labBullshitNum: cc.Label, //反对数

        page1: cc.Node, //三个推荐的文章链接
        page2: cc.Node, 
        page3: cc.Node, 

        readNum: cc.Label,   //阅读量
        labAuthor: cc.Label,  //作者
        lotteryPagePrefab: cc.Prefab,  //新闻列表预制

        _homeType: null
    },

    // use this for initialization
    onLoad: function () {

    },

    init: function(home,tem,openType){
        this._homeType =home;
        this._openType =openType;
        if(this.node.getChildByName('content').getChildByName('ndDoc').active){
            //-----
            var newsid =null;
            if(tem.Data){
                this.webView.url =tem.Data.LookUrl;
                newsid = tem.Data.ID;
            }else{
                this.webView.url =tem.LookUrl;
                newsid = tem.ID;
            }
            
            //红球号 蓝球号
            var recv = function(ret){
                //ComponentsUtils.unblock();
                cc.log('ret',ret);
                if (ret.Code !== 0) {
                    console.log(ret.Msg);
                    ComponentsUtils.showTips(ret.Msg);
                }else{ 
    
                    //----
                    this.newPage(ret); 
                }
            }.bind(this);
            cc.log('查看资讯'+newsid);
            var data = {
                Token:User.getLoginToken(),
                NewsID: newsid,
            };
            CL.HTTP.sendRequest(DEFINE.HTTP_MESSAGE.POSTNEWSANALY, data, recv.bind(this),"POST"); 
        }

    },


    //关闭
    onClose:function(event){
        if(this._openType){
            var canvas = cc.find("Canvas");
            var lotterys =cc.instantiate(this.lotteryPagePrefab); 
            canvas.addChild(lotterys); 
            lotterys.getComponent('lotteryPage').init(this._homeType,this._homeType._newsDataList);
        }
        //-----
        this.node.getComponent("Page").backAndRemove();

    },

    //当前新闻界面显示
    newPage: function(ret,url){
        this._newId =ret.Data.NewsData[0].ID;
        this.labHead.string = ret.Data.Title;
        //var str ='                            ';
        this.labTimeNameNum.string = ret.Data.Time;  
        this.labAuthor.string = ret.Data.Author;
        this.readNum.string = ret.Data.ReadNum;     
        if(url !=undefined){
            this.webView.url =url;
        }

        var numsArry = ret.Data.LotNumber.split("|");
        
        var _red ='', _blue =''
        for(var i=0;i<numsArry[0].split(",").length;++i){
            var path ="red"+numsArry[0].split(",")[i];
            _red += "<img src='"+path+"'/>"+' ';
        }
        for(var i=0;i<numsArry[1].split(",").length;++i){
            var path ="blue"+numsArry[1].split(",")[i];
            _blue += "<img src='"+path+"'/>"+' ';
        }
       
        this.labRed.string = _red;    //numsArry[0];
        this.labBlue.string = _blue;  //numsArry[1];

        this.labReasonNum.string = ret.Data.SupportNum;
        this.labBullshitNum.string = ret.Data.OpposeNum;

        this.page1.active =false;
        this.page2.active =false;
        this.page3.active =false;
        var pageArr =[this.page1,this.page2,this.page3]
        for(var i=0;i<ret.Data.NewsData.length;++i){
            pageArr[i].active =true;
            pageArr[i].getComponent(cc.Label).string = '●'+ret.Data.NewsData[i].Title;   
        }

        this._newsDataList = ret.Data.NewsData;   

        //----将推荐的红蓝球号码存起来
        this._redBlueNum = numsArry;
    },

    splitOne:function(num){
        return Number(num);
    },

    //投注按钮
    onBet: function(){
        if(this._homeType.node.getChildByName('newsPage')){
            this._homeType.node.getChildByName('newsPage').removeFromParent(); 
        }
        this._homeType.betByNewsCallBack(this._redBlueNum);
        this._openType =false;
        this.onClose();  
    },

    //切换推荐的文章页面
    onGoToPage: function(event,customData){
  
        var recv = function(ret){
            if (ret.Code !== 0) {
                console.log(ret.Msg);
                ComponentsUtils.showTips(ret.Msg);
            }else{ 
                ComponentsUtils.showTips("发送成功!");  
                //----
                this.newPage(ret,this._newsDataList[Number(customData)].LookUrl); 
            }
        }.bind(this);

        var data = {
            Token:User.getLoginToken(),
            NewsID: this._newsDataList[Number(customData)].ID,
        };
        CL.HTTP.sendRequest(DEFINE.HTTP_MESSAGE.POSTNEWSANALY, data, recv.bind(this),"POST"); 

    },

    //更多按钮
    onBtnMore: function(){
        var can =cc.find('Canvas');
        var lotterys =cc.instantiate(this.lotteryPagePrefab);
        this._homeType.node.addChild(lotterys); 
        lotterys.getComponent('lotteryPage').init(this._homeType,this._homeType._newsDataList);
        this.node.getComponent("Page").backAndRemove();
    },

    onReason: function(){

        var recv = function(ret){
            //ComponentsUtils.unblock();
            if (ret.Code !== 0) {
                console.log(ret.Msg);
                ComponentsUtils.showTips(ret.Msg);
            }else{ 
                ComponentsUtils.showTips("发送成功!");   
            }
        }.bind(this);

        var data = {
            Token:User.getLoginToken(),
            NewsID: this._newId,
            IsSupport: 1
        };
        CL.HTTP.sendRequest(DEFINE.HTTP_MESSAGE.POSTNEWSEVALUATE, data, recv.bind(this),"POST"); 
    },

    onBullshit: function(){
        var recv = function(ret){
            //ComponentsUtils.unblock();
            
            if (ret.Code !== 0) {
                console.log(ret.Msg);
                ComponentsUtils.showTips(ret.Msg);
            }else{ 
               // ComponentsUtils.showTips("发送成功!");   
            }
        }.bind(this);

        var data = {
            Token:User.getLoginToken(),
            NewsID: this._newId,
            IsSupport: 0
        };
        CL.HTTP.sendRequest(DEFINE.HTTP_MESSAGE.POSTNEWSEVALUATE, data, recv.bind(this),"POST");     
    },

    // called every frame, uncomment this function to activate update callback
    // update: function (dt) {

    // },
});
